package noobbot;

public class TurboEnd {
	public final String msgType;
	public final HWCarId data;
	public final Object gameTick;
	public TurboEnd(String msgType, HWCarId data, Object gameTick) {
		super();
		this.msgType = msgType;
		this.data = data;
		this.gameTick = gameTick;
	}
}
