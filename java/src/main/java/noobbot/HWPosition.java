package noobbot;

public class HWPosition {
	public final HWCarId id;
	public final double angle;
	public final HWPiecePosition piecePosition;
	public HWPosition(HWCarId id, double angle, HWPiecePosition piecePosition) {
		super();
		this.id = id;
		this.angle = angle;
		this.piecePosition = piecePosition;
	}
}
