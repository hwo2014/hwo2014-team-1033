package noobbot;

public class YourCar {
	public final String msgType;
	public final HWCarId data;
	public YourCar(String msgType, HWCarId data) {
		super();
		this.msgType = msgType;
		this.data = data;
	}
}
