package noobbot;

public class HWLane {
	public final int distanceFromCenter;
	public final int index;
	
	public HWLane(int distanceFromCenter, int index) {
		super();
		this.distanceFromCenter = distanceFromCenter;
		this.index = index;
	}

}
