package noobbot;

public class JoinRace extends SendMsg {
	public final BotId botId;
	public final String trackName;
	public final String password;
	public final int carCount;

	@Override
	protected String msgType() {
		return "joinRace";
	}

	public JoinRace(BotId botId, String trackName, String password, int carCount) {
		super();
		this.botId = botId;
		this.trackName = trackName;
		this.password = password;
		this.carCount = carCount;
	}

}

class BotId {
	public final String name;
	public final String key;
	public BotId(String name, String key) {
		super();
		this.name = name;
		this.key = key;
	}
}
