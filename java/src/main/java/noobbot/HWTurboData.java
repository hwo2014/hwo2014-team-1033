package noobbot;

public class HWTurboData {
	public final double turboDurationMilliseconds;
	public final int turboDurationTicks;
	public final double turboFactor;
	public HWTurboData(double turboDurationMilliseconds, int turboDurationTicks, double turboFactor) {
		super();
		this.turboDurationMilliseconds = turboDurationMilliseconds;
		this.turboDurationTicks = turboDurationTicks;
		this.turboFactor = turboFactor;
	}
	
}
