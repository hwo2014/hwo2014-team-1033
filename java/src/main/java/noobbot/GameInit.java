package noobbot;

public class GameInit {
	public final String msgType;
	public final HWData data;
	public GameInit(String msgType, HWData data) {
		super();
		this.msgType = msgType;
		this.data = data;
	}
}