package noobbot;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.gson.Gson;

public class Main {
    public static void main(String... args) throws IOException {
        String host = args[0];
        int port = Integer.parseInt(args[1]);
        String botName = args[2];
        String botKey = args[3];

        System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

        final Socket socket = new Socket(host, port);
        final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));

        final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));

        new Main(reader, writer, new Join(botName, botKey));
//        new Main(reader, writer, new JoinRace(new BotId(botName, botKey), "france", null, 1));
    }

    final Gson gson = new Gson();
    private PrintWriter writer;
    

    public Main(final BufferedReader reader, final PrintWriter writer, final SendMsg join) throws IOException {
        this.writer = writer;
        String line = null;
        
        GameInit gameInit;
        
        int turboDurationTicks = 0;
    	double turboFactor = 0.0;
    	boolean turboUsed = false;
    	int turboStartTick = 0;
    	String myBotColor = "";
    	
    	int longestStraight = -1;
        
        List<TickData> tickData = new ArrayList<>(5000);
        
        Map<Integer, HWLane> lanes = new HashMap<>();
        
        TickData lastTickData = null;
        HWPosition lastPosition = null;
        
        Map<Integer, HWPiece> trackPieces = new HashMap<>();
        int lastPieceIndex = 0;
        
        Map<Integer, Integer> trackPieceIndexToNearestSwitchPieceIndex = new HashMap<>();
        
        int index = 0;
        double lastThrottle = 0.0;
        boolean switchedBetweenCurrentSwitches = false;
        int changeLine = 0;

        send(join);

        while((line = reader.readLine()) != null) {
            final MsgWrapper msgFromServer = gson.fromJson(line, MsgWrapper.class);
            if (msgFromServer.msgType.equals("carPositions")) {
            	final CarPositions carPositions = gson.fromJson(line, CarPositions.class);
            	HWPosition position = null;
            	for (HWPosition p : carPositions.data) {
            		if (p.id.color.equals(myBotColor)) {
            			position = p;
            			break;
            		}
            	}
            	index = position.piecePosition.pieceIndex;
            	
            	if (trackPieces.get(index).swwitch != null) {
            		switchedBetweenCurrentSwitches = false;
            		changeLine = 0;
            	}
            	
            	if (lastPosition == null) {
            		lastPosition = position;
            	}
            	
            	double distance;
        		if (lastPosition.piecePosition.pieceIndex == position.piecePosition.pieceIndex) {
        			distance = position.piecePosition.inPieceDistance - lastPosition.piecePosition.inPieceDistance;
        		} else {
        			HWPiece lastPiece = trackPieces.get(lastPosition.piecePosition.pieceIndex);
        			if (lastPiece.angle == null) {
        				distance = lastPiece.length - lastPosition.piecePosition.inPieceDistance;
        			} else {
        				int laneIndex = position.piecePosition.lane.startLaneIndex;
        				
        				if (lastPiece.angle > 0) {
        					double length = 2*Math.PI*(lastPiece.radius-lanes.get(laneIndex).distanceFromCenter)/360*Math.abs(lastPiece.angle);
        					distance = length - lastPosition.piecePosition.inPieceDistance;
        				} else {
        					double length = 2*Math.PI*(lastPiece.radius+lanes.get(laneIndex).distanceFromCenter)/360*Math.abs(lastPiece.angle);
        					distance = length - lastPosition.piecePosition.inPieceDistance;
        				}
        			}
    				distance = distance + position.piecePosition.inPieceDistance;
        		}
            	
            	HWPiece actualPiece = trackPieces.get(position.piecePosition.pieceIndex);
            	double throttle=0.0;
            	if (actualPiece.angle == null) {
            		if ((index == (longestStraight)) && (lastThrottle == 1.0) && (turboDurationTicks != 0) && (position.piecePosition.lap > 0) && !turboUsed) {
            			turboUsed = true;
            			turboStartTick = carPositions.gameTick;
            			System.out.println("USING TURBO at tick "+carPositions.gameTick);
            			send(new TurboMsg("Pow pow pow pow pow, or your of personalized turbo message"));
            		}
            		throttle = 1.0;
            	} else {
            		int laneIndex = position.piecePosition.lane.startLaneIndex;
            		double radius;
            		if (actualPiece.angle > 0) {
    					radius = actualPiece.radius-lanes.get(laneIndex).distanceFromCenter;
    				} else {
    					radius = actualPiece.radius+lanes.get(laneIndex).distanceFromCenter;
    				}
            		
            		double allowedSpeed = radius/25+2.8;
            		if (radius > 95) {
            			allowedSpeed = radius/25+2.8;
            		}
            		if (radius > 110) {
            			allowedSpeed = radius/100+7.4;
            		}
            		throttle = allowedSpeed/10;
            		if (throttle > 1.0) {
            			throttle = 1.0;
            		}
            		if ((distance/10 + 0.02) < throttle) {
            			throttle = 1.0;
            		}
            		
            		double difference = (distance/10) - throttle;
            		if (difference > 0.05) {
            			throttle = 0.0;
            		} else {
            			if (difference > 0) {
            				throttle = 0.1;
            			}
            		}
            	}
            	
            	int oneAheadPieceIndex = getAheadPieceIndex(index, 1, lastPieceIndex);
        		HWPiece oneAheadPiece = trackPieces.get(oneAheadPieceIndex);
        		if (oneAheadPiece.angle != null) {
        			int laneIndex = position.piecePosition.lane.startLaneIndex;
        			if ((trackPieceIndexToNearestSwitchPieceIndex.get(index) < oneAheadPieceIndex) && (switchedBetweenCurrentSwitches)) {
        				laneIndex = laneIndex + changeLine;
        			}
            		double radius;
            		if (oneAheadPiece.angle > 0) {
    					radius = oneAheadPiece.radius-lanes.get(laneIndex).distanceFromCenter;
    				} else {
    					radius = oneAheadPiece.radius+lanes.get(laneIndex).distanceFromCenter;
    				}
            		double futureSpeed = radius/25+2.8;
            		if (radius > 95) {
            			futureSpeed = radius/25+2.8;
            		}
            		if (radius > 110) {
            			futureSpeed = radius/100+7.4;
            		}
            		futureSpeed = futureSpeed/10;
            		
            		double distanceToPiece = getDistanceToPiece(index, position.piecePosition.inPieceDistance, oneAheadPieceIndex, trackPieces, lastPieceIndex);
            		
            		double speedDifference = (distance/10) - futureSpeed;
            		
            		if ((speedDifference > 0) && (speedDifference < 0.05)) {
            			if (throttle > 0.5) {
            				throttle = 0.1;
            			}
            		} else {
	            		if ((distanceToPiece/700 < speedDifference)) {
	            			throttle = 0.0;
	            		}
            		}
        		}
            	
            	for (int i = 2; i < 6; i++) {
            		int aheadPieceIndex = getAheadPieceIndex(index, i, lastPieceIndex);
            		HWPiece aheadPiece = trackPieces.get(aheadPieceIndex);
            		if (aheadPiece.angle != null) {
            			int laneIndex = position.piecePosition.lane.startLaneIndex;
            			if ((trackPieceIndexToNearestSwitchPieceIndex.get(index) < aheadPieceIndex) && (switchedBetweenCurrentSwitches)) {
            				laneIndex = laneIndex + changeLine;
            			}
                		double radius;
                		if (aheadPiece.angle > 0) {
        					radius = aheadPiece.radius-lanes.get(laneIndex).distanceFromCenter;
        				} else {
        					radius = aheadPiece.radius+lanes.get(laneIndex).distanceFromCenter;
        				}
                		double futureSpeed = radius/25+2.8;
                		if (radius > 95) {
                			futureSpeed = radius/25+2.8;
                		}
                		if (radius > 110) {
                			futureSpeed = radius/100+7.4;
                		}
                		futureSpeed = futureSpeed/10;
                		
                		double distanceToPiece = getDistanceToPiece(index, position.piecePosition.inPieceDistance, aheadPieceIndex, trackPieces, lastPieceIndex);
                		
                		double speedDifference = (distance/10) - futureSpeed;
                		
                		if (needToBrake(distanceToPiece, speedDifference)) {
                			throttle = 0.0;
                			break;
                		}                		
            		}
            	}
            	
            	
            	lastPosition = position;
            	
            	TickData tick;
            	int laneIndex = position.piecePosition.lane.startLaneIndex;
        		double radius;
        		if (actualPiece.angle == null) {
        			radius = 0;
        		} else {
	        		if (actualPiece.angle > 0) {
						radius = actualPiece.radius-lanes.get(laneIndex).distanceFromCenter;
					} else {
						radius = actualPiece.radius+lanes.get(laneIndex).distanceFromCenter;
					}
        		}
            	if (lastTickData == null) {
            		tick = new TickData(carPositions.gameTick, position.angle, 0, 0, 0, index, throttle, radius);
            	} else {
            		double velocity = distance - lastTickData.distance;
            		tick = new TickData(carPositions.gameTick, position.angle, distance, velocity, velocity - lastTickData.velocity, index, throttle, radius);
            	}
            	
            	lastTickData = tick;
            	
            	tickData.add(tick);
            	
            	if ((Math.abs(throttle - lastThrottle) < 0.001) && (trackPieces.get(index).swwitch == null) && (!switchedBetweenCurrentSwitches)) {
            		int nearestSwitch = trackPieceIndexToNearestSwitchPieceIndex.get(index);
            		String switchDirection = getSwitchDirection(trackPieces, nearestSwitch, trackPieceIndexToNearestSwitchPieceIndex.get(nearestSwitch), position.piecePosition.lane.startLaneIndex, lanes, lastPieceIndex);
            		if (switchDirection != null) {
            			switchedBetweenCurrentSwitches = true;
            			if (switchDirection.equals("Right")) {
            				changeLine = 1;
            			} else {
            				changeLine = -1;
            			}
            			send(new Switch(switchDirection));
            			continue;
            		}
            	}
            	
            	lastThrottle = throttle;
                send(new Throttle(throttle));
            } else if (msgFromServer.msgType.equals("crash")) {
                System.out.println("Crashed at piece "+index);
                send(new Throttle(1.0));
            } else if (msgFromServer.msgType.equals("join")) {
                System.out.println("Joined");
            } else if (msgFromServer.msgType.equals("gameInit")) {
            	gameInit = gson.fromJson(line, GameInit.class);
            	List<Integer> switchIndexes = new ArrayList<>();
            	int i = 0;
            	ArrayList<Integer> straightindexes = new ArrayList<>();
            	for (HWPiece piece : gameInit.data.race.track.pieces) {
            		if (piece.length == null) {
            			double length = 2*Math.PI*piece.radius/360*Math.abs(piece.angle);
            			HWPiece newPiece = new HWPiece(length, piece.radius, piece.angle, piece.swwitch);
            			trackPieces.put(i, newPiece);
            		} else {
            			trackPieces.put(i, piece);
            			straightindexes.add(i);
            		}
            		if (piece.swwitch != null) {
            			switchIndexes.add(i);
            		}
            		i++;
            	}
            	lastPieceIndex = i-1;
            	
            	int currentStart = 0;
            	int currentCount = 0;
            	int maxCount = 0;
            	int maxStart = 0;
            	int lastNumber = -2;
            	for (Integer in : straightindexes) {
            		if ((in-1) == lastNumber) {
            			currentCount++;
            			lastNumber = in;
            		} else {
            			if (maxCount < currentCount) {
	            			maxCount = currentCount;
	            			maxStart = currentStart;
            			}
            			currentCount = 1;
            			currentStart = in;
            			lastNumber = in;
            		}
            	}
            	
            	if (maxCount < currentCount) {
        			maxCount = currentCount;
        			maxStart = currentStart;
    			}
            	
            	if ((straightindexes.get(0) == 0) && (straightindexes.get(straightindexes.size()-1) == lastPieceIndex)) {
            		int connectedcount = 2;
            		int j;
            		for (j = 1; j < straightindexes.size(); j++) {
            			if (straightindexes.get(j) == j) {
            				connectedcount++;
            			} else {
            				break;
            			}
            		}
            		int piecIndex;
            		for (j = straightindexes.size()-2, piecIndex = lastPieceIndex-1; j > 0; j--, piecIndex--) {
            			if (straightindexes.get(j) == piecIndex) {
            				connectedcount++;
            			} else {
            				break;
            			}
            		}
            		if (connectedcount > maxCount) {
            			maxStart = piecIndex+1;
            		}
            	}
            	
            	
            	longestStraight = maxStart;
            	
            	int switchIndex = 0;
            	int swwitch = switchIndexes.get(switchIndex);
            	boolean lastSwitch = false;
            	
            	for (i = 0; i <= lastPieceIndex; i++) {
            		if (lastSwitch) {
            			trackPieceIndexToNearestSwitchPieceIndex.put(i, swwitch);
            		} else {
	            		if (i < swwitch) {
	            			trackPieceIndexToNearestSwitchPieceIndex.put(i, swwitch);
	            		} else {
	            			switchIndex++;
	            			if ((switchIndexes.size()-1) == switchIndex) {
	            				swwitch = switchIndexes.get(0);
	            				trackPieceIndexToNearestSwitchPieceIndex.put(i, swwitch);
	            				lastSwitch = true;
	            			} else {
	            				swwitch = switchIndexes.get(switchIndex);
	            				trackPieceIndexToNearestSwitchPieceIndex.put(i, swwitch);
	            			}
	            		}
            		}
            	}
            	
            	for (HWLane lane : gameInit.data.race.track.lanes) {
            		lanes.put(lane.index, lane);
            	}
            	send(new Throttle(1.0));
                System.out.println("Race init");
            } else if (msgFromServer.msgType.equals("gameEnd")) {
                System.out.println("Race end");
//                for (TickData tick : tickData) {
//                	System.out.println(tick.tickCount+","+tick.distance+","+tick.velocity+","+tick.carAngle+","+tick.pieceIndex+","+tick.throttle+","+tick.pieceAngle);
//                }
            } else if (msgFromServer.msgType.equals("gameStart")) {
                System.out.println("Race start");
                send(new Throttle(1.0));
            } else if (msgFromServer.msgType.equals("yourCar")) {
                YourCar car = gson.fromJson(line, YourCar.class);
                System.out.println("MyCar");
                myBotColor = car.data.color;
                send(new Throttle(1.0));
            } else if (msgFromServer.msgType.equals("turboEnd")) {
            	TurboEnd tend = gson.fromJson(line, TurboEnd.class);
            	send(new Throttle(lastThrottle));
                System.out.println("TURBO end");
                if (tend.data.color.equals(myBotColor)) {
                	turboUsed = false;
                }
            } else if (msgFromServer.msgType.equals("turboAvailable")) {
            	Turbo turbo = gson.fromJson(line, Turbo.class);
            	turboDurationTicks = turbo.data.turboDurationTicks;
            	turboFactor = turbo.data.turboFactor;
            	send(new Throttle(lastThrottle));
            	System.out.println("TURBOO "+longestStraight);
            } else {
            	send(new Throttle(lastThrottle));
            }
        }
    }
    
    private String getSwitchDirection(Map<Integer, HWPiece> trackPieces, int nearestSwitch, Integer nextSwitch, int laneIndex, Map<Integer, HWLane> lanes, int lastPieceIndex) {
    	String result = null;
		double currentLine = calculateTimeForLine(trackPieces, nearestSwitch, nextSwitch, laneIndex, lastPieceIndex, lanes);
		double rightLine = 0;
		double leftLine = 0;
		
		if (lanes.get(laneIndex+1) != null) {
			rightLine = calculateTimeForLine(trackPieces, nearestSwitch, nextSwitch, laneIndex+1, lastPieceIndex, lanes);
			if (rightLine < currentLine) {
				result = "Right";
				currentLine = rightLine;
			}
		}
		
		if (lanes.get(laneIndex-1) != null) {
			leftLine = calculateTimeForLine(trackPieces, nearestSwitch, nextSwitch, laneIndex-1, lastPieceIndex, lanes);
			if (leftLine < currentLine) {
				result = "Left";
				currentLine = leftLine;
			}
		}
		
		return result;
	}

	private double calculateTimeForLine(Map<Integer, HWPiece> trackPieces, int nearestSwitch, Integer nextSwitch, int laneIndex, int lastPieceIndex, Map<Integer, HWLane> lanes) {
		double result = 0.0;
		if (nextSwitch > nearestSwitch) {
			for (int i = nearestSwitch; i < nextSwitch; i++) {
				HWPiece piece = trackPieces.get(i);
				if (piece.angle != null) {
					double radius;
            		if (piece.angle > 0) {
    					radius = piece.radius-lanes.get(laneIndex).distanceFromCenter;
    				} else {
    					radius = piece.radius+lanes.get(laneIndex).distanceFromCenter;
    				}
            		
            		double allowedSpeed = radius/25+2.8;
            		if (radius > 95) {
            			allowedSpeed = radius/25+2.8;
            		}
            		if (radius > 110) {
            			allowedSpeed = radius/100+7.4;
            		}
            		double length = 2*Math.PI*radius/360*Math.abs(piece.angle);
            		double time = length / allowedSpeed;
            		result = result + time;
				}
			}
		} else {
			for (int i = nearestSwitch; i <= lastPieceIndex; i++) {
				HWPiece piece = trackPieces.get(i);
				if (piece.angle != null) {
					double radius;
            		if (piece.angle > 0) {
    					radius = piece.radius-lanes.get(laneIndex).distanceFromCenter;
    				} else {
    					radius = piece.radius+lanes.get(laneIndex).distanceFromCenter;
    				}
            		
            		double allowedSpeed = radius/25+2.8;
            		if (radius > 95) {
            			allowedSpeed = radius/25+2.8;
            		}
            		if (radius > 110) {
            			allowedSpeed = radius/100+7.4;
            		}
            		double length = 2*Math.PI*radius/360*Math.abs(piece.angle);
            		double time = length / allowedSpeed;
            		result = result + time;
				}
			}
			for (int i = 0; i < nextSwitch; i++) {
				HWPiece piece = trackPieces.get(i);
				if (piece.angle != null) {
					double radius;
            		if (piece.angle > 0) {
    					radius = piece.radius-lanes.get(laneIndex).distanceFromCenter;
    				} else {
    					radius = piece.radius+lanes.get(laneIndex).distanceFromCenter;
    				}
            		
            		double allowedSpeed = radius/25+2.8;
            		if (radius > 95) {
            			allowedSpeed = radius/25+2.8;
            		}
            		if (radius > 110) {
            			allowedSpeed = radius/100+7.4;
            		}
            		double length = 2*Math.PI*radius/360*Math.abs(piece.angle);
            		double time = length / allowedSpeed;
            		result = result + time;
				}
			}
		}
		return result;
	}

	//TODO lines + angles
    private double getDistanceToPiece(int currentPieceIndex, double currentInPieceDistance, int destPieceIndex, Map<Integer, HWPiece> trackPieces, int maxIndexCount) {
    	double result = 0.0;
    	
    	result = result + trackPieces.get(currentPieceIndex).length - currentInPieceDistance;
    	
    	if (destPieceIndex > currentPieceIndex) {
    		for (int i = (currentPieceIndex+1); i < destPieceIndex; i++) {
    			result = result + trackPieces.get(i).length;
    		}
    	} else {
    		for (int i = (currentPieceIndex+1); i <= maxIndexCount; i++) {
    			result = result + trackPieces.get(i).length;
    		}
    		for (int i = 0; i < destPieceIndex; i++) {
    			result = result + trackPieces.get(i).length;
    		}
    	}
    	
    	return result;
    }
    
    private boolean needToBrake(double distanceToPiece, double speedDifference) {
    	return (distanceToPiece/700 < speedDifference);
    }
    
    private int getAheadPieceIndex(int current, int count, int maxIndexCount) {
    	int result = current + count;
    	if (result > maxIndexCount) {
    		result = result - (maxIndexCount+1);
    	}
    	return result;
    }


	private void send(final SendMsg msg) {
        writer.println(msg.toJson());
        writer.flush();
    }
}
