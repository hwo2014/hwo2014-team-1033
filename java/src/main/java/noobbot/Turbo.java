package noobbot;

public class Turbo {
	public final String msgType;
	public final HWTurboData data;
	public Turbo(String msgType, HWTurboData data) {
		super();
		this.msgType = msgType;
		this.data = data;
	}
}
