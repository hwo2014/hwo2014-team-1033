package noobbot;

public class HWRace {
	public final HWTrack track;
	public final Object cars;
	public final Object raceSession;
	public HWRace(HWTrack track, Object cars, Object raceSession) {
		super();
		this.track = track;
		this.cars = cars;
		this.raceSession = raceSession;
	}
	
}