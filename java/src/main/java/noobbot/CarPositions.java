package noobbot;

import java.util.List;

public class CarPositions {
	public final String msgType;
	public final List<HWPosition> data;
	public final int gameTick;
	public CarPositions(String msgType, List<HWPosition> data, int gameTick) {
		super();
		this.msgType = msgType;
		this.data = data;
		this.gameTick = gameTick;
	}
}
