package noobbot;

public class TickData {

	public int tickCount;
	public double carAngle;
	public double distance;
	public double velocity;
	public double acceleration;
	public int pieceIndex;
	public double throttle;
	public Double pieceAngle;
	public TickData(int tickCount, double carAngle, double distance, double velocity, double acceleration, int pieceIndex, double throttle, Double pieceAngle) {
		super();
		this.tickCount = tickCount;
		this.carAngle = carAngle;
		this.distance = distance;
		this.velocity = velocity;
		this.acceleration = acceleration;
		this.pieceIndex = pieceIndex;
		this.throttle = throttle;
		this.pieceAngle = pieceAngle;
	}
	
	
}
