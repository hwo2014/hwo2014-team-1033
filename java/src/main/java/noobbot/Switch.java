package noobbot;

public class Switch extends SendMsg {
	private String data;
	
	public Switch(String data) {
        this.data = data;
    }

    @Override
    protected Object msgData() {
        return data;
    }

	@Override
	protected String msgType() {
		return "switchLane";
	}
	
}