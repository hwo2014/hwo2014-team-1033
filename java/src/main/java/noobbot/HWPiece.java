package noobbot;

import ch.qos.logback.core.util.AggregationType;

import com.google.gson.annotations.SerializedName;

public class HWPiece {
	public Double length;
	public Double radius;
	public Double angle;
	@SerializedName(value="switch")
	public Boolean swwitch;
	
	public HWPiece(Double length, Double radius, Double angle, Boolean swwitch) {
		super();
		this.length = length;
		this.radius = radius;
		this.angle = angle;
		this.swwitch = swwitch;
	}

	public HWPiece(double length) {
		this(length, null, null, null);
	}

	public HWPiece(double length, boolean swwitch) {
		this(length, null, null, swwitch);
	}

	public HWPiece(double radius, double angle) {
		this(null, radius, angle, null);
	}

	public HWPiece(double radius, double angle, boolean swwitch) {
		this(null, radius, angle, swwitch);
	}

	
	@Override
	public String toString() {
		return "Piece [length= "+length+", radius= "+radius+", angle= "+angle+", switch= "+swwitch+"]";
	}
}
