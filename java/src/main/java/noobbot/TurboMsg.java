package noobbot;

public class TurboMsg extends SendMsg {
	private String data;

    public TurboMsg(String data) {
        this.data = data;
    }

    @Override
    protected Object msgData() {
        return data;
    }

    @Override
    protected String msgType() {
        return "turbo";
    }

}
