package noobbot;

import java.util.List;

public class HWTrack {
	public final String id;
	public final String name;
	public final List<HWPiece> pieces;
	public final List<HWLane> lanes;
	public HWTrack(String id, String name, List<HWPiece> pieces, List<HWLane> lanes) {
		super();
		this.id = id;
		this.name = name;
		this.pieces = pieces;
		this.lanes = lanes;
	}
}