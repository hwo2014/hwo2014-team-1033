package noobbot;

public class HWPiecePosition {
	public final int pieceIndex;
	public final double inPieceDistance;
	public final HWPieceLane lane;
	public final int lap;
	public HWPiecePosition(int pieceIndex, double inPieceDistance, HWPieceLane lane, int lap) {
		super();
		this.pieceIndex = pieceIndex;
		this.inPieceDistance = inPieceDistance;
		this.lane = lane;
		this.lap = lap;
	}
	
}